﻿using MobileDemo.Models;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileDemo
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListViewPage : ContentPage, INotifyPropertyChanged
    {
        DataService dataService = new DataService();

        ObservableCollection<Contacto> _listaLocal;
        public ObservableCollection<Contacto> ListaLocal
        {
            get { return _listaLocal; }
            set
            {
                if(_listaLocal != value)
                {
                    _listaLocal = value;
                    OnPropertyChanged(nameof(Load));
                }    
            }
        }

        private void Load()
        {
            List1.ItemsSource = App.Lista;
        }

        public ListViewPage()
        {
            InitializeComponent();

            try
            {
                App.Lista =
                new ObservableCollection<Contacto>();

                //App.Lista.Add(new Contacto()
                //{
                //    Nombre = "Ana Vazques",
                //    Ocupacion = "Manager",
                //    Imagen = "icon.png",
                //    Telefono = 1234598
                //});
                //App.Lista.Add(new Contacto()
                //{
                //    Nombre = "Brandon Olivares",
                //    Ocupacion = "Developer",
                //    Imagen = "icon.png",
                //    Telefono = 78459415
                //});
                //App.Lista.Add(new Contacto()
                //{
                //    Nombre = "Maria Luna",
                //    Ocupacion = "Admin",
                //    Imagen = "icon.png",
                //    Telefono = 545451
                //});

                var result = dataService.GetContactos();
                if (result != null)
                {
                    if (result.Count > 0)
                    {
                        //recuperar los registros de la BD y mantenelos en la Var Global
                        App.Lista =
                            new ObservableCollection<Contacto>(result);
                    }
                    else
                    {
                        //registrar nuevo
                        foreach (var item in App.Lista)
                        {
                            dataService.InsertContacto(item);
                        }
                    }
                }
                else
                {
                    //registrar nuevo
                    foreach (var item in App.Lista)
                    {
                        dataService.InsertContacto(item);
                    }
                }

                List1.ItemsSource = null;
                List1.ItemsSource = App.Lista;
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }

        }

        public async void List_ItemSelected(object sender,
            SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;
            await Application.Current.MainPage.Navigation.PushAsync(
                new ModificarContactoPage((Contacto)e.SelectedItem));
        }


        protected async void ToolbarItem_Clicked(object sender, EventArgs e)
        {
            try
            {
                //await Application.Current.MainPage.DisplayAlert("Test", "Mensaje", "Aceptar");
                //App.Lista.Add(new Contacts() { Name = "Lorenzo Ramos", Occupation = "Cliente", Image = "icon.png" });

                await Navigation.PushAsync(new NuevoContactoPage());
                List1.ItemsSource = App.Lista;


            }
            catch (Exception err)
            {
                await DisplayAlert(
                       "Error", err.Message,
                       "Aceptar");
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            List1.ItemsSource = null;
            List1.ItemsSource = App.Lista;
        }

        #region Singleton
        private static ListViewPage instance;
        public static ListViewPage GetInstance()
        {
            if(instance == null)
            {
                instance = new ListViewPage();
            }
            return instance;
        }
        #endregion
    }
}