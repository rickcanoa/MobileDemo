﻿using System;
using System.Collections.Generic;
using System.Linq;
using MobileDemo.Models;
using SQLite.Net;
using Xamarin.Forms;

namespace MobileDemo
{
    public class DataAccess : IDisposable
    {
        private SQLiteConnection connection;
        public DataAccess()
        {
            try
            {
                var config = DependencyService.Get<IConfig>();
                connection = new SQLiteConnection(config.Platform,
                    System.IO.Path.Combine(config.DirectoryDB, "Xamarin.db3"));
                connection.CreateTable<Contacto>();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public void Insert<T>(T model)
        {
            connection.Insert(model);
        }

        public void Update<T>(T model)
        {
            connection.Update(model);
        }

        public void Delete<T>(T model)
        {
            connection.Delete(model);
        }

        public T Find<T>(int id) where T : class
        {
            return connection.Table<T>().FirstOrDefault(model => model.GetHashCode() == id);
        }

        public T First<T>() where T : class
        {
            return connection.Table<T>().FirstOrDefault();
        }

        public List<T> GetList<T>() where T : class
        {
            return connection.Table<T>().ToList();
        }

        public void Dispose()
        {
            connection.Dispose();
        }
    }
}
