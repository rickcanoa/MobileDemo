﻿using MobileDemo.Models;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace MobileDemo
{
    public partial class App : Application
	{
        public static ObservableCollection<Contacto> Lista { get; set; }

        public App ()
		{
			InitializeComponent();

			//MainPage = new ListViewPage();
            MainPage = new NavigationPage(new ListViewPage());
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
