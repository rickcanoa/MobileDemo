﻿using SQLite.Net.Attributes;

namespace MobileDemo.Models
{
    public class Contacto
    {
        [PrimaryKey, AutoIncrement]
        public int IdContacto { get; set; }
        public string Nombre { get; set; }
        public string Ocupacion { get; set; }
        public string Imagen { get; set; }
        public int Telefono { get; set; }
    }
}
