﻿using MobileDemo.Models;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileDemo
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NuevoContactoPage : ContentPage
	{
        DataService dataService = new DataService();
        public NuevoContactoPage ()
		{
			InitializeComponent ();
		}
        private async void Button_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtTelefono.Text))
                {
                    await DisplayAlert(
                        "Error", "El valor en Telefono es requerido",
                        "Aceptar");
                    return;
                }

                Contacto con = new Contacto()
                {
                    Nombre = txtNombre.Text,
                    Ocupacion = txtOcupacion.Text,
                    Imagen = "icon.png",
                    Telefono = int.Parse(txtTelefono.Text)
                };

                App.Lista.Add(con);

                dataService.InsertContacto(con);

                await Navigation.PopAsync();
            }
            catch (Exception err)
            {
                //throw new Exception(err.Message);
                await DisplayAlert(
                       "Error", err.Message,
                       "Aceptar");
            }

        }
    }
}