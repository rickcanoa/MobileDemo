﻿using System;
using System.Collections.Generic;
using System.Linq;
using MobileDemo.Models;
using Xamarin.Forms;

namespace MobileDemo
{
    public partial class ModificarContactoPage : ContentPage
    {
        public Contacto _contacts { get; set; }
        DataService dataService = new DataService();

        public ModificarContactoPage(Contacto contacts)
        {
            InitializeComponent();
            this._contacts = contacts;

            txtNombre.Text = contacts.Nombre;
            txtOcupacion.Text = contacts.Ocupacion;
            txtTelefono.Text = contacts.Telefono.ToString();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtTelefono.Text))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debe ingresar un valor", "Aceptar");
                return;
            }

            //Recuperar el valor para poder 
            var con = App.Lista.Where(l => l.Nombre.Equals(_contacts.Nombre)).FirstOrDefault();
            if (con != null)
            {
                con.Ocupacion = txtOcupacion.Text;
                dataService.UpdateContacto(con);
            }

            var aux = ListViewPage.GetInstance();
            aux.ListaLocal = App.Lista;
            await Application.Current.MainPage.Navigation.PopAsync();
        }
    }
}
