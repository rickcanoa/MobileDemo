﻿using System;
using SQLite.Net.Interop;

namespace MobileDemo
{
    public interface IConfig
    {
        string DirectoryDB { get;  }
        ISQLitePlatform Platform { get;  }
    }
}