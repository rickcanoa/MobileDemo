﻿using System;
using System.Collections.Generic;
using MobileDemo.Models;

namespace MobileDemo
{
    public class DataService
    {
        public Contacto GetContacto()
        {
            try
            {
                using (var da = new DataAccess())
                {
                    return da.First<Contacto>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public List<Contacto> GetContactos()
        {
            try
            {
                using (var da = new DataAccess())
                {
                    return da.GetList<Contacto>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Response InsertContacto(Contacto contacto)
        {
            try
            {
                using (var da = new DataAccess())
                {
                    //Inserta el nuevo
                    da.Insert(contacto);
                }
                return new Response
                {
                    IsSuccess = true,
                    Message = "Contacto Registrado",
                    Result = contacto
                };
            }
            catch (Exception err)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = err.Message
                };
            }
        }
        public Response UpdateContacto(Contacto contacto)
        {
            try
            {
                using (var da = new DataAccess())
                {
                    da.Update(contacto);
                }
                return new Response
                {
                    IsSuccess = true,
                    Message = "Contacto Actualizado OK",
                    Result = contacto
                };
            }
            catch (Exception err)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = err.Message
                };
            }
        }
        public void DeleteContacto()
        {
            try
            {
                using (var da = new DataAccess())
                {
                    //Eliminar el Anterior
                    var oldContacto = da.First<Contacto>();
                    if (oldContacto != null)
                        da.Delete(oldContacto);
                }
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
        }
    }
}
